function oneThroughTwenty() {
    for (let counter = 1; counter <=20; counter++) {
        console.log(counter);
    }
}
oneThroughTwenty();
    
 //call function oneThroughTwenty
 
 function evensToTwenty() {
   for (let counter = 1; counter <=20; counter++) {
    if (counter % 2 == 0) {
      console.log(counter) ;
   }
  }
}
  evensToTwenty();
 
 //call function evensToTwenty
 
 function oddsToTwenty() {
   for (let counter = 1; counter <=20; counter++) {
     if (counter % 2 !== 0) {
       console.log(counter) ;
     }
    }
  }
  oddsToTwenty();
 
 //call function oddsToTwenty
 
 function multiplesOfFive() {
   for (let counter = 5; counter <=100; counter++) {
     if (counter % 5 == 0) {
       console.log(counter) ;
     }
    }
  }
  multiplesOfFive();
     
 //call function multiplesOfFive
 
 function squareNumbers() {
   for (let counter = 1; counter <=100; counter++) {
     let n = 1
     n = (counter * counter);
      console.log(n);
   }
  }
  squareNumbers()
 
 //call function squareNumbers
 
 function countingBackwards() {
   for (let counter = 20; counter >= 1; counter--) {
     console.log(counter);
  }
}
  countingBackwards()
 
 //call function countingBackwards
 
 function evenNumbersBackwards() {
   for (let counter = 20; counter >= 1; counter--) {
     if (counter % 2 == 0) {
       console.log(counter);
     }
    }
  }
  evenNumbersBackwards()
 
 //call function evenNumbersBackwards
 
 function oddNumbersBackwards() {
   for (let counter = 20; counter >= 1; counter--) {
     if (counter % 2 !== 0) {
       console.log(counter); 
     }
    }
  }
  oddNumbersBackwards()
 
 //call function oddNumbersBackwards
 
 function multiplesOfFiveBackwards() {
   for (let counter = 100; counter >= 1; counter--) {
     if (counter % 5 == 0)
     {
       console.log(counter);
     }
    }
  }
  multiplesOfFiveBackwards()
 
 //call function multiplesOfFiveBackwards
 
 function squareNumbersBackwards() {
   for (let counter = 100; counter >= 1; counter--) {
     let n = 0
     n = (counter * counter)
     console.log(n);
   }
  }
  squareNumbersBackwards()